/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
import java.util.*;
public class CardTrick {
    
    public static void main(String[] args)
    {
        Scanner kb = new Scanner(System.in);
        Card[] magicHand = new Card[7];
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            //c.setValue(insert call to random number generator here)
            c.setValue((int)(Math.random()*13)+1);
            
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
            c.setSuit(Card.SUITS[((int)(Math.random()*4))]);
            magicHand[i]=c;
        }
        
        //insert code to ask the user for Card value and suit, create their card
        Card c2 = new Card();
        System.out.println("Enter card value");
        int value = kb.nextInt();
        System.out.println("Enter card suit");
        String suit = kb.next();
         c2.setValue(value);
         c2.setSuit(suit);
        // and search magicHand here
        boolean checker = false; 
        
        for (int i=0; i<magicHand.length; i++){
            System.out.print(magicHand[i].getValue());
            System.out.println(magicHand[i].getSuit());
            if(magicHand[i].getSuit()==suit&&magicHand[i].getValue()==value){
//saying if the suit of the card is the same as the suit the user enter and the same thing goes for the value
        checker = true;
        
                }
        
        //Then report the result here
      
            
    }
            //Then report the result here
        if(checker==true){
        System.out.println("Your card is in the deck");
        
        }
        else{
            System.out.println("Your card is not in the deck");
        }
        
}
}
